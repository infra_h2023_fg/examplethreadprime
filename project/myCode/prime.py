import threading
import multiprocessing
import random
import time
import math

def isPrime(n):
    #edge cases
    if n == 0 or n == 1:
        return False   
    end = n #TODO use a smarter end bound
    for i in range(2, end):
        if n%i == 0:
            return False
    return True

def getPrimeElements(numberList):
    rep = []
    for number in numberList:
        if isPrime(number):
            rep.append(number)
    print(rep)


def run1(numberList):
    getPrimeElements(numberList)

def run2(numberList):
    thread1 = threading.Thread(target=getPrimeElements, args=(numberList,))
    thread2 = threading.Thread(target=getPrimeElements, args=(numberList,))
    thread3 = threading.Thread(target=getPrimeElements, args=(numberList,))
    thread1.start()
    thread2.start()
    thread3.start()
    thread1.join()
    thread2.join()
    thread3.join()


def run3(numberList):
    p1 = multiprocessing.Process(target=getPrimeElements, args=(numberList,))
    p2 = multiprocessing.Process(target=getPrimeElements, args=(numberList,))
    p3 = multiprocessing.Process(target=getPrimeElements, args=(numberList,))
    p1.start()
    p2.start()
    p3.start()
    p1.join()
    p2.join()
    p3.join()



if __name__ == '__main__':
    numberList = []
    for n in range (60000):
        numberList.append(random.randint(100, 100000))
    
    startTime = time.time()  

    run1(numberList)
    #run2(numberList)
    #run3(numberList)

    
    endTime = time.time()
    print("program completed.")
    print("Computation time is " + str(endTime - startTime) + " seconds")
